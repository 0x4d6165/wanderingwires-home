:PROPERTIES:
:ID:       115fe7c8-96c6-482b-85a1-7a0cc81f0d28
:END:
#+title: Julie McBride
#+filetags: :OCs:writing:


** Bio
- Born in Cork, Ireland in 1904, as of 1930 she lives in Boston, Massachusetts
- Pronouns: she/it. Gender: non-binary trans woman (pretty tomboyish/butch). Sexuality: bisexual.
- Has gray eyes and greasy straight (or maybe slightly wavy) black hair
- Is on HRT (just started existing in the 20s)
- Smells like stale coffee, old leather, and cigarettes (jacket smells faintly of "metal")
- [[file:julie.png][a picrew of Julie McBride]
- Wears blood-stained (steel-toed) combat boots, and always wears her black leather jacket
- Wears several rings on each hand, and a small silver medallion of Jude the Apostle
- TIRED
- Dissociates frequently but not as much as she used to (as of the Gwendolyn campaign)
- Autistic
- She'd really value any queer community/friends she could get
- Keeps a jet-black rosary in the breast pocket of her leather jacket
- Likes: edgy bullshit, horror/gothic literature (loves Edgar Allen Poe), cute animals (cats, rabbits, puppies), deals (wants to make better deals than she made before), expensive liquor, spliffs (and just weed and tobacco in general), coffee (no matter how stale, cold, burnt, cheap, etc. it is), juice, leather, rich food, red wine, beer, Shakespeare, gothic cathedrals, jazz (goes to jazz bars to decompress after kills), writing super edgy poetry, baseball
- Dislikes: cops, coercion, feeling controlled or confined, most vegetables (someone please get her a multivitamin),
- Struggles with asking for/accepting help
- Has a scrunkly edgy rat fursona.
- She eats her own boogers
- She orders her steak black and blue
- Has been known to have a cigarette and cup of black coffee for breakfast sometimes (often)
- She LOVES spicy food and is super competitive about it
- Speaks Polari
- Modern AU Julie is the front woman for a hardcore band and a guitarist in an emo band (both small, underground bands). The hardcore band is called "Julie and the Murder Suspects" because Julie is incapable of being normal
- Modern AU Julie rides a motorcycle
- Always "tests" the doorknob 5 times after locking it
- Claude is the only person who knows Julie is trans and vice versa and they switched names (so Julie is Claude's deadname and vice versa)
- once punched a priest who pissed her off by being homophobic (she felt really bad about it afterwards though)
- Has OCD (manifests sometimes as her washing her hands raw and bleeding because she can't get the metaphorical blood off)
  ## General ideas
- Julie McBride from Prentice ttrpg backstory in the syndicate based on the protag of Three Cheers for Sweet Revenge
- Kill 1,000 evil men maybe? Do hits for the syndicate? Killing (rival?) syndicate members. 1,000th is the boss?
  - *Maybe her partner was going to be killed by the syndicate and this was the deal she made to prevent that? Maybe the boss didn't expect her to actually succeed and was like "oh fuck"*
- *Wanna have her have some super scary like omen dream personifying her guilt that pushes her to run away from the syndicate (Drowning in a shoreless sea of blood or something edgy like that)*
- https://youtu.be/bqFhzodjpp8 Also this feels relevant lol
- BLOOD (symbolic and sexy)
- ONLY wears black or red (says she's "mourning")
  - maybe wears red gloves when feeling extra edgy
- Constantly narrowly escapes death and capture. She thinks God is toying with her. (God's not done with her yet.)
  - Gets mysteriously brought back to life many times?
- *Does the sign of the cross when she kills*
- During Prentice's campaign in the Hall of Regrets all Julie sees is a shoreless sea of blood with a jet black, starless sky
- +Dresses up as a vampire for Halloween every year because she's incapable of being normal+
- Is obsessed with a certain saint, not sure which yet. Maybe Jude the Apostle? or Mary Magdalene
- First couple lines of "Hail Mary" carved into her gun
- She saves a pet rat in the aftermath who keeps her company while she Endures
- GF was named Zelda Sharpe (she/they/he maybe? Not sure yet) and was also bi and was Julie's literal partner in crime
  - She only wears white and blue
  - Since Julie struggles accepting help, it did most of the kills by itself until Zelda made it let her help
- She gets really soft and vulnerable when someone truly empathizes and connects with her despite her hard exterior
- Refers to her victims by number ("The 10th," "The 107th")
  - Maybe certain victims are "boss victims"? Wanna make The 1,000th one of them and maybe someone she meets throughout the 1,000
- Despite being a career criminal before the 1,000, she made a point to never kill anyone before or after.
- Writes "case files" on the kills and fics are formatted like these case files because keeping a document of these people that are gone because of Julie helps her cope somewhat.
- Eventually realizes she needs to live for herself and not others, even people she loves (because living for yourself helps you love them)
- Gets nicknamed "Cherry Bomb" by fellow syndicate members/bullies on the playground
- Julie leather jacket metaphor
  - When she feels especially unsafe she sleeps under it like blanket and during the day she'll never take it off
  - When she's more comfortable she's more likely to take it off though not for too long prob
- Evil Julie AU? She's a Perfect Christian Girl™️ who's a wealthy socialite, very conventionally attractive, cares a lot about keeping up appearances, and is extremely self-righteous and judgmental. She is "forced" into killing the 1,000 but it's obviously just an excuse for something she's always wanted to do and takes a great deal of sadistic pleasure in. You can't fix her. (And if you try that's sus.)
- "locks are just a polite do not open sign"
- Fallout New Vegas AU? might do play through as this AU
- i feel like it could also be fun to have post return zelda love a food she hated before and when julie is like ??? zelda points out that julie used to love black pudding and now she hates it
- Proficient with many weapons, prefers her trusty old revolver
  ## Fic ideas
- First kill? Or maybe one that seems very routine at first till it's not
- The whole fic is the aftermath (clean the scene and get outta town) of a kill
- "I realize most people probably don't keep a tally of their sins in a blood-stained leather notebook."
- "The rest of my life is a fucking funeral"
- "I fucking hate getting blood out of carpet."
- "Oh god they had a fucking family." (Julie interrupts herself while making her getaway to think about this)
- Not sure what happens to the girlfriend? Maybe when Julie tells The Boss she killed The Thousandth he kills the girlfriend and that's when she kills him? Also what if this was the first person The Boss has killed personally (in effect, his "first kill").
- After she finds out The Boss kills Zelda she has a scene trying to get him on the payphone set to that part of "Bury Me in Black" where they go "PICK UP THE PHONE, FUCKER" and the "insides" part of that song as well.
- Fic titled "An Appeal To Heaven" when she moves to Boston (based on the MA bay flag) and it's about her learning how to be a Person again after The Horrors
- Fic title/motif "crime doesn't pay"
- Fic titled "John 8:7"
- Meeting death? ultimately making a deal with them for Zelda
- AU Julie fic where Luci Disenchantment is her personal demon
- Scene set to "Holding out for a Hero" where Julie is coming to save Zelda and it's very queer
- Fic called "Which one's Pink?" about the relationship between Julie and The Boss during The 1,000 relates to "Have a Cigar" by Pink Floyd
- Title "War-Worn Lipstick"
- okay what if it writes a julie fic that takes place on ash wednesday and she's running around doing her thing with a big ash smudge on her forehead
- tries to take communion after the first few kills and she finds she can't bring herself to lift it to her mouth and runs out of the church crying

** Misc
- [[file:Blender.org][Blender]] 3D model of the blood sea with the words from "Bury Me in Black,"
  - “These eyes have had too much // To drink again, tonight +// Black skies, we'll douse ourselves // In high explosive lies+
- The Julie TV show (if it ever existed) would have an unnervingly chipper intro song maybe?
- [[file:Julie_McBride.pdf][Julie McBride's Character Sheet]]
